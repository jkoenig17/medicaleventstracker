//
//  AddEventViewController.swift
//  MedicalEventsTracker
//
//  Created by James Koenig on 9/12/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import UIKit

protocol AddEventDelegate {
   func addEvent(event:Event)
}

class AddEventViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate {
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var dateTextView: UITextField!
    
    var selectedPickerViewItem: String?
    var id = 31
    var delegate: AddEventDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let dateString = Formatter.iso8601.string(from: Date())
        dateTextView.text = dateString
        dateTextView.delegate = self
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch row {
        case 0: selectedPickerViewItem =  MedicationEnum.albuterol.rawValue
        case 1: selectedPickerViewItem = MedicationEnum.proair.rawValue
        case 2: selectedPickerViewItem = MedicationEnum.symbicort.rawValue
        default: break
            //Shouldn't happen
            
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return MedicationEnum.allValues.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch row {
        case 0: return MedicationEnum.albuterol.rawValue
        case 1: return MedicationEnum.proair.rawValue
        case 2: return MedicationEnum.symbicort.rawValue
        default:
            //Shouldnt happen
            return "Oops"
        }
        
        
    }
    
    // MARK: IBActions
    
    @IBAction func addEvent(sender: Any) {
        guard let selectedItem = selectedPickerViewItem else{return
        }
        
        let medType = self.getMedicationType(medicationName: MedicationEnum(rawValue: selectedItem)!)
        
        let event: Event = Event(datetime: dateTextView.text!, medicationName: MedicationEnum(rawValue: selectedItem)!, medicationType: Medicationtype(rawValue: medType)!, id: self.id)
        //Add event to table View
        self.delegate?.addEvent(event: event)
        
        self.id = self.id + 1
        
        DispatchQueue.main.async {
            
        
        //Show alert
        let alert = UIAlertController(title: "Event Added!", message: "Your event has been added", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        self.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func getMedicationType(medicationName:MedicationEnum) -> String {
        switch medicationName {
        case .proair:
            return Medicationtype.rescue.rawValue
        case .symbicort:
            return Medicationtype.controller.rawValue
        case .albuterol:
            return Medicationtype.rescue.rawValue
        }
    }
    
    // MARK: Keyboard
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
