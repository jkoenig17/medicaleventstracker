//
//  EventCell.swift
//  MedicalEventsTracker
//
//  Created by James Koenig on 9/12/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import UIKit

class EventCell: UITableViewCell {

    @IBOutlet weak var id: UILabel!
    @IBOutlet weak var medicationName: UILabel!
    @IBOutlet weak var medicationType: UILabel!
    @IBOutlet weak var timestamp: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
