//
//  EventsDAO.swift
//  MedicalEventsTracker
//
//  Created by James Koenig on 9/12/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import Foundation

//Closures for Events network Access
typealias EventsSearchSuccess = (UserEventsResponse?) -> Void

struct EventsDao {
    
    func getEvents(success: @escaping EventsSearchSuccess) {
      
        // Set up the URL request
        let todoEndpoint: String = "https://rawgit.com/jbmoss84/2d985dc9e3a78721e1c8231d605a3f03/raw/16eba334c36efc2d43a4931bf5ddc4ac9a98a840/codable.json"
        
        guard let url = URL(string: todoEndpoint) else {
            print("Error: cannot create URL")
            return
        }
        let urlRequest = URLRequest(url: url)
        
        // set up the session
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        let task = session.dataTask(with: urlRequest) {data, response, error in
            //check for error
            if error != nil {
                print("Unable to complete network request")
            } else {
                guard let givenData = data else {
                    print("Unable to unwrap data in Events search success")
                    return
                }
                
                //decode data
                do {
                    let jsonDecoder = JSONDecoder()
                    let eventsSearchResponse = try jsonDecoder.decode(UserEventsResponse.self, from: givenData)
                    
                    success(eventsSearchResponse)
                } catch {
                    print("Unable to decode UserEvents Response")
                    return
                }
            }
        }
        task.resume()
    }
}
