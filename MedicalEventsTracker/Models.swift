//
//  User.swift
//  MedicalEventsTracker
//
//  Created by James Koenig on 9/12/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import Foundation

struct UserEventsResponse: Codable {
    let user: User
    let events: [Event]
}

struct Event: Codable {
    let uid: String
    let datetime: String
    let medication: MedicationEnum
    let medicationtype: Medicationtype
    let id: Int
    
    init(datetime: String, medicationName: MedicationEnum, medicationType: Medicationtype, id: Int) {
        self.uid = ""
        self.datetime = datetime
        self.medication = medicationName
        self.medicationtype = medicationType
        self.id = id
    }
}

enum MedicationEnum: String, Codable {
    case albuterol = "albuterol"
    case proair = "proair"
    case symbicort = "symbicort"
    
    static let allValues = [albuterol, proair, symbicort]
    
    
}

enum Medicationtype: String, Codable {
    case controller = "controller"
    case rescue = "rescue"
}

struct User: Codable {
    let name, address1, address2: String
    let uid: String
    let sex, dob, disease: String
    let medications: [MedicationElement]
}

struct MedicationElement: Codable {
    let name: MedicationEnum
    let medicationtype: Medicationtype
}
