//
//  ViewController.swift
//  MedicalEventsTracker
//
//  Created by James Koenig on 9/12/18.
//  Copyright © 2018 James Koenig. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AddEventDelegate {
    
    var events: [Event] = []
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let eventsDao = EventsDao()
        var userEventsResponse: UserEventsResponse? = nil
        eventsDao.getEvents(success: { (success) in
            userEventsResponse = success
            if let pulledEvents = userEventsResponse?.events, let pulledUser = userEventsResponse?.user.name{
                self.events = pulledEvents
                DispatchQueue.main.async {
                    self.events.sort() { self.convertISO8601StringToDate(date: $0.datetime) < self.convertISO8601StringToDate(date: $1.datetime) }
                    self.tableView.reloadData()
                    self.navigationItem.title = pulledUser
                }
            }
            
            
            
        })
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: Table View
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return events.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Events"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let event = events[indexPath.row]
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "EventCell") as? EventCell {
            cell.medicationName.text = event.medication.rawValue
            cell.medicationType.text = event.medicationtype.rawValue
            cell.id.text = String(event.id)
            cell.timestamp.text = event.datetime
            return cell
        }
        return UITableViewCell()
    }
    
    func convertISO8601StringToDate(date:String) -> Date {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZZ"

        guard let date = dateFormatter.date(from: date) else {return Date()}
        
        return date
    }
    
    func addEvent(event: Event) {
        
        self.events.append(event)
        
        DispatchQueue.main.async {
            self.events.sort() { self.convertISO8601StringToDate(date: $0.datetime) < self.convertISO8601StringToDate(date: $1.datetime) }
            self.tableView.reloadData()
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let controller = segue.destination as? AddEventViewController {
            controller.delegate = self
        }
    }
}

extension Formatter {
    static let iso8601: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime, .withFractionalSeconds]
        return formatter
    }()
}

